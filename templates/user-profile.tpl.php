<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 * <?php print render($user_profile['field_']); ?>
 */
?>
<div class="profile <?php print $attributes; ?>">

	<div class="profile-wrap block">
		
		<div class="profile-top">
			<div class="profile-pic"><?php print render($user_profile['user_picture']); ?></div>
			<div class="profile-initial">
				<a href="#" id="link-full-profile" class="tiny button secondary"><span class="p-show">Show</span><span class="p-hide">Hide</span> profile</a>
			</div><!-- /.profile-initial -->
		</div><!-- /.profile-top -->
		
		<div style="clear:both;"></div>
		
		
		<div class="profile-full">
			<div class="col personal">
				<?php print render($user_profile['uid']); ?>
				<?php print render($user_profile['field_first_name']); ?>
				<?php print render($user_profile['field_last_name']); ?>
				<?php print render($user_profile['field_middle_name']); ?>
				<?php print render($user_profile['mail']); ?>
				<?php print render($user_profile['field_secondary_email']); ?>
				<?php print render($user_profile['field_address']); ?> 
			</div>
			<div class="col work-location">
			
			<div class="field">
			<div class="field-label">County</div>
			<?php print render($user_profile['group_work_location']['field_county'][0]); ?>
			</div>
			<div class="field">
			<div class="field-label">District</div>
			<?php print render($user_profile['group_work_location']['field_district_term'][0]); ?>
			</div>
			<div class="field">
			<div class="field-label">School</div>
			<?php print render($user_profile['group_work_location']['field_school'][0]); ?>
			</div>
			<?php print render($user_profile['group_work_location']['field_school_level']); ?>
			<?php print render($user_profile['group_work_location']['field_site_address']); ?>
			</div>
			<div class="col">
			<div class="cohort field"><div class="field-label">Cohort:</div> <?php print render($user_profile['group_cohort']['field_participation_year'][0]); ?> <?php print render($user_profile['group_cohort']['field_grouping'][0]); ?></div>
			
			</div>
		
			<div style="clear:both;"></div>
	
	    	<div class="profile-documents-all">
	    		<h3 class="subheader">Documents</h3>
				<div class="columns-wrap">
					<div class="col profile-documents">
						<?php print render($user_profile['group_profile_documents']['field_casc_enrollment_form']); ?>
						<?php print render($user_profile['group_profile_documents']['field_fcoe_casc_agreements']); ?>
						<?php print render($user_profile['group_profile_documents']['field_verification_of_employment']); ?>
					</div>
					<div class="col profile-documents">
						<?php print render($user_profile['group_profile_documents']['field_position_profile']); ?>
						<?php print render($user_profile['group_profile_documents']['field_job_shadow_reflection']); ?>
					</div>
					<div class="col profile-documents">
						<?php print render($user_profile['group_profile_documents']['field_pd_application_activities']); ?>
					</div>
				</div><!-- /.columns-wrap -->
			</div><!-- /.profile-docs-all -->
		
			<div style="clear:both;"></div>
	    
	    </div><!-- /.profile-full -->
	
	</div><!-- /.profile-wrap -->
	
	
</div><!-- /.profile -->
