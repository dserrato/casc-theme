// (function ($) {$(document).ready(function() {

(function ($, Drupal, window, document, undefined) {

$(document).ready(function(){
          $(".toggler").click(function(){
            //$(this).parent('tr').find('.field-name-field-dops-evidence').toggle("fast");
            //$(this).parent().parent().parent().parent().find('.field-name-field-dops-evidence').toggle();
            //$(this).parents('tr').find('.field-name-field-dops-evidence').toggle();
            $(this).find('.field-name-field-dops-evidence').fadeToggle();
            return false;
          });
        $('.view-id-ilp_reflection tr').each(function() {
        	//$(this).hide();
        	var row = $(this);
        	row.find('.question').first().appendTo(row.find('th'));
      	});
      	$('#link-full-profile').toggle(function() {
			$(this).children('.p-show').hide();
			$(this).children('.p-hide').show();      	
      	},
      	function() {
      		$(this).children('.p-show').show();
      		$(this).children('.p-hide').hide();
      	}
      	).toggle(function() {
      	  $('.profile-full').slideDown();
      	  
      	},
      	function() {
      	  $('.profile-full').slideUp();
      	});
 
 if ($(".page-node-add-ilp-reflection, .page-user-reflection").length) {
 $(document).scroll(function() {
        var top = $(document).scrollTop();
        if (top > 300) $('.hidden').show();
        if (top < 300) $('.hidden').hide();
      	});
      	var div = $('.hidden');
      	var start = $(div).offset().top;
	    $.event.add(window, "scroll", function() {
        var p = $(window).scrollTop();
        $(div).css('position',((p)>start) ? 'fixed' : 'static');
        $(div).css('top',((p)>start) ? '30px' : '');
    	});
    }
      
  if ($(".page-user").length) {
   $(".profile-initial").prepend($("#page-title"));
  }
  
  
  $('.my-list').easyListSplitter({ colNumber: 3 });

  $('.node-ilp_reflection-form .form-radio[value=_none]').parent().hide();
    
 });
 
 


})(jQuery, Drupal, this, this.document); 
 
// });})(jQuery);

